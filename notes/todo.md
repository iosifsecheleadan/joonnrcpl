# Code

- Start learning about ANTLR
- Create the grammar

# Documentation

- Cross References:
	- everywhere, reference to next documentation file
	- references to useful files
- Split documentation into packages: 
	- jon.bultin : what is buit into the language
	- jon.language : additional basic language features
	- jon.collections : 
		- jon.collections.basic : the four basic collections
	- jon.unitTest : library for unit testing (see below)

## Unit Test
- command line utility : 
```
jonu	-package path/to/test/package
		-class path/to/testClass
		-method path/to/testClass:testMethod 
		-indir <input/directory>[:<another/input/directory>:etc]
```
- at least one of package, class, method must be provided
- if package is provided, all classes in package and contained packages are tested
- if class is provided, all methods of class are tested
- if method is provided, only the specific method of the class is provided
- indir just like with the basic cli
- given classes must be static
- they must extend TestClass, and optionally implement beforeAll, beforeEach, afterAll, afterEach
- any other public methods with no arguments are considered test methods
- if a method fails, all other methods are run
- you need file reader functionality for this
- improve this with reflection

## Libraries

When implementing libraries, see also 
[ISO for Information technology](https://www.iso.org/committee/45020/x/catalogue/)