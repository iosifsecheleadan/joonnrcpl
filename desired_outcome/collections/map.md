# 5.5. Map

An abstract collection based on key, value pairs. Keys are unique. 

Quick access to any value, based on key. Easy on memory management. Quick 
search. 

Implemented concretely as a Hash Map (Hash Table).

```
add(key, value)
remove(key)
get(key)
has(key)
size()
```