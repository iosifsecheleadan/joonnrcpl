# 3.1. Comments

The use of comments is highly discouraged. You should rather spend time naming 
your classes, methods and variables in an intuitive way, rather than write 
comments throughout your code. If you want to learn more, see Clean Code by 
Robert C. Martin.

There are two types of comments: 
1. One line
2. Multi line

### 3.1.1. One line comments

```
// Like in many other programming languages, comments can be written like this
// Pretty intuitive, huh? 
```

### 3.1.2. Multi line comments

```
/*	Again, like in many other programming languages, comments can also be 
	written like this.
*/
```

```
/* This is also a valid, 
 * and more pretty, format.
 */
```

```
/* Nested comments are not supported though
/* So this comment ends here */

*/	// and this is a syntax error 
	// there's no such thing as multiplicative division
```

### Comments on same line as code

```
    // if there's only white space surround a comment
		/* that's fine, no problems */
alpha = "a"; // but since comments are statements, they need to be separated
beta = "b"; /* from other code by a newline or semicolon */
```