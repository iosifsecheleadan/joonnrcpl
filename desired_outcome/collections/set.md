# 5.4. Set

An abstract collection based on uniqueness of elements. 

Access only through iteration. Easy on memory management. Quick search.

Implemented concretely as a Hash Set (Hash Table).

```
add(object)
remove(object)
has(object)
size()
```