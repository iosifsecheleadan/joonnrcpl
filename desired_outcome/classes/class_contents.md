# 2. Classes - Contents

<!-- todo references to specific files -->
<!-- todo everywhere, references to next doc file -->

```
[import statements]
<class declaration>
```

Any jon file consists of optional import statements, and a class declaration.
To learn more, also see Import Statements.

1. Classes
2. Enumerations
3. Fields
4. Constructors
5. Methods
