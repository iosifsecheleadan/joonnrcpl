# 3.10. Continue

```
continue
```

The continue statement is used to abruptly terminate the current iteration of a 
for or while loop. When reached, execution is resumed from the first statement 
in the loop (with the next element in the collection for the for loop).