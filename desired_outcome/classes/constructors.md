# 2.4. Constructors

Constructors are special methods of classes that initialize an object. 

```
[private / protected / public] [abstract/override] ClassName([Type1 arg1, Type2 arg2, etc]) {
	[statements]
}
```

## Constructor access modifiers

- Private: Only accessible from within the class
- Protected: Only accessible from classes extending this class
- Public: Accessible from any class
- Default: Public

## Other constructor descriptors

### Abstract

Abstract constructors may only be declared inside abstract classes. These 
constrcutors force the concrete class to provide an implementation for the 
constructor.

### Override

The implementation provided in the subclass must have the override descriptor. 
A method cannot be both abstract (not implemented) and overridden (implemented) 
in the same class. 

Moreover, if there are multiple methods in super classes, with the same 
signature, the concrete class will override them, providing it's own 
implementation. This implementation must have the override descriptor. 

## Object instantiation

```
variableName = ClassName([arguments])
```

To learn more about arguments, see Method Arguments.

<!-- todo references to specific files -->
