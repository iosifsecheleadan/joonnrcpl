# 4. Basic Types

All basic types are: 
- potentially infinitely large
- immutable (their values cannot be changed - new objects are created with the 
new values)

Any class is a subclass of the default Object class.

1. Object
2. Arithmetic
3. Relational
4. Logical
5. Integer
6. Float
7. Boolean
8. Character
9. String
10. Exception

## Constant auto-boxing

Any constant in the code is automatically converted to an object of it's 
respective type. The following examples are valid for Float, Boolean, Character, 
String as well as Integer.

The following statements are equivalent:
```
integer = 5
integer = Integer(5)
```

Also, this nonsensical statement is valid:
```
5.add(6)
```
Which would seem to add `6` to the constant `5`. Does this mean that now 
wherever we use the constant `5`, it's value is actually `11`? 

No, because as we have seen above, `5` is actually equivalent to `Integer(5)`. 
So the previous code translates to: 
```
Integer(5).add(Integer(6))
```
Which is equivalent to: 
```
5 + 6
```
A statement that is useless without an assignment. So how about: 
```
eleven = 5 + 6
```
