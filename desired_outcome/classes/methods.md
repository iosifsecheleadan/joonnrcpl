# 2.5. Methods

```
[private / protected / public] [abstract/override] <ReturnType> <methodName>([Type1 arg1, Type2 arg2]) {
	[statements]
}
```

## Method access modifiers

- Private: Only accessible from within the class
- Protected: Only accessible from classes extending this class
- Public: Accessible from any class
- Default: Public

When a class extends another class, it may override public or protected methods.
Indeed, it must override abstract methods and duplicate methods. 

To learn more, see Extending other classes.

<!-- todo references to specific files -->

## Other method descriptors

### Abstract

Abstract methods may only be declared inside abstract classes. These methods 
cannot be implemented in this class. An implementation must be provided in the 
subclass.

### Override

The implementation provided in the subclass must have the override descriptor. 
A method cannot be both abstract (not implemented) and overridden (implemented) 
in the same class. 

Moreover, if there are multiple methods in super classes, with the same 
signature, the concrete class will override them, providing it's own 
implementation. This implementation must have the override descriptor. 

## Return Type

A method must declare it's return type or `void` if it lacks one. If a method 
has a return type it must return, and it must return an object of that type. If 
a method is declared as void, it may not return any object. 

Variables may be declared, or their values can be changed, using the return 
object.

## Method Name

Should contain only alphanumerical characters and _. Must start with a letter.

Recommended to be camelCase starting with a lower case letter. 

## Method Arguments

A method may have any number of arguments. These arguments behave just as 
variables in the function body, but they are instantiated with the passing 
value. 

To learn more about the function body, see Statements.

<!-- todo references to specific files -->
