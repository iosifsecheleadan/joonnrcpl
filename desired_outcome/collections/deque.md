# 5.3. Deque

An abstract collection based on end and beginning access.

Quick access to head and tail, no direct access to rest of list. Easy on memory 
management. Slow search.

Implemented concretely as a Doubly Linked List.

```
pushFirst(object) // O(1)
pushLast(object) // O(1)
popFirst(object) // O(1)
popLast(object) // O(1)
has(object) // O(n)
size() // O(1)
```
