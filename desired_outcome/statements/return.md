# 3.8. Return

```
return [object]
```

The return statement is used to exit a method with an optional value. 

The returned value must be of the declared type of the method. This value can 
then be assigned to a variable. 

If the method type is `void`, the return statement cannot have a value, and the 
result of the method (i.e. nothing) cannot be assigned to a variable. 