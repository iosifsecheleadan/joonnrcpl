# 4.5. Integer

Integer is a concrete implementation of the Arithmetic and Relational types. 
When an Integer is used in an arithmetic or relational expression with a Float, 
it is automatically cast to Float.

Besides these, Integer also has the special `%` (modulo - rest of division) 
operator. This operation can only be done between Integer types.

Holds positive and negative whole numbers, that is, non-fractional numbers.

Examples: -3546765434, -987, -3, -1, -0, 0, 1, 9, 234, 9876543234

## Underscores

Underscores can also be inserted into the numbers for visibility. 

Valid: -3_546_765_434, 9_876_543_234, 1_2_3_4_5, 12_3_45

Invalid: _-123, -_123, -123_

