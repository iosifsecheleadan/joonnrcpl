# 4.8. Character

Character is a concrete implementation of Relational type. It represents 
unicode characters and it's toString() method returns their textual 
representation. 

It also has an implementation of the `+` and `-` operations. These, and the 
relational operations relate to the unicode numbers that the characters 
represent. Therefore you can add an Integer to a Character, but not vice versa. 
Going out of bounds results in an exception. 

The syntax for assigning a character is 
```
variableName = '[any one unicode character]'
```

Multiple characters between "" create a String. 