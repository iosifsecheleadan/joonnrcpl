# Iterable 

Abstract type with the following abstract functions, used silently in for loops: 

```
void beginIterator()
Type goToNext()
```

Any collection must extend this class. The `goToNext()` method sets the iterator 
to the next element, and returns it. When at the end of the collection, it 
throws and `EndOfCollectionException` which brings it out of the for loop. 

All collection modifying methods (add, remove, etc), must also make sure the 
iterator remains consistent. 
