# 3.3. Assignment

```
variableName = expressionStatement
```

If the named variable doesn't yet exist, a new variable is created, with the 
type of the right-hand expression. This type is always known at compile time, 
either because it is a basic type, a constructor or a method - in which case 
the type is found in the method signature. 

## Variable name

Should contain only alphanumerical characters and _. Must start with a letter.

Recommended to be camelCase starting with a lower case letter. 

## Arithmetic Assignment

```
addition += 5
subtraction -= 5
multiplication *= 5
division /= 5
addition += 5
modulus %= 5
power ^= 5
```

This is a combination between an assignment and an expression statement. The 
above code translates to: 

```
addition = addition + 5
subtraction = subtraction - 5
multiplication = multiplication * 5
division = division / 5
modulus = modulus % 5
power = power ^ 5
```

So, each line is an Assignment Statement, where the right hand side is an 
arithmetic expression. 