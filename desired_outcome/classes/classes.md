# 2.1. The class declaration

```
[private/public] [abstract/static] class <ClassName> [: SuperClass1, SuperClass2, ...] {
	[enumeration]
	[fields]
	[constructors]
	[methods]
}
```

Enumerations, fields, constructors and methods can be declared in any order in 
a class, though it is recommended to declare them in the above order, for 
readability.

## Class access modifiers

- Private: Only visible in package
- Public: Visible from any package
- Default: Public

## Other class descriptors

### Concrete

Lacking the abstract or static descriptor, a class is called concrete. A 
concrete class may be instantiated. It may also be extended. It may not have 
abstract methods.

### Abstract

An abstract class may not be instantiated. It may be extended, and the concrete 
extension may be instantiated.

An abstract class may have abstract methods. See method syntax.
<!-- todo reference to method syntax -->

### Static

A static class cannot have any constructors, therefore no instances of this 
class may exist. Class enumeration, fields and methods may be accessed and 
called. They are all static (though the static keyword doesn't need to be 
repeated). Usage example:

```
variable2 = StaticClass.ENUMERATION
variable1 += StaticClass.field
StaticClass.method()
```

## Class name

Should contain only alphanumerical characters and _. Must start with a letter.

Recommended to be CamelCase starting with an upper case letter.

## Extending other classes

In ***jon***, a class may extend multiple other classes. This means it inherits 
all public and protected enumerations, fields and methods of the superclass. 

A concrete class must implement all abstract methods of super classes.
If there are multiple methods in super classes, with the same signature, the 
concrete class will override them, providing it's own implementation. 

Inheriting Enumerations or Fields with the same name results in a compiler 
error. 

Enums, fields and methods of super classes are accessible through the 
`super<ClassName>.` keyword. 

Thus there are no interfaces, only abstract classes.
