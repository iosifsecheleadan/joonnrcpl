# 5.2. List

An abstract collection based on index access. 

Quick access to any element. Heavy on memory management. Slow search.

Implemented concretely as an Dynamic Array.

```
add(object) // O(1)
insert(object, Integer position) // O(n)
remove(object) // O(n)
remove(Integer fromPosition, Integer toPosition) // O(n)
get(Integer position) // O(1)
has(object) // O(n)
size() // O(1)
```
