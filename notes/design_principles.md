1. Everything is a class: 
	- try to avoid introducing new concepts into the language for your 
	convenience
	- the more concepts there are, the steeper the learning curve
2. Syntax sugar
	- beautify the statements (over time)
	- create constant to object mappings (or code construct to object)
		- 5 -> Integer(5)
		- "gaga" -> String("gaga")
		- { [statements] } -> Function([statements])
		- etc... 