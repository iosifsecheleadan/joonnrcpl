# ANTLR

## Short pages or articles

- [Official Websie](https://antlr.org)
- [Getting started](https://github.com/antlr/antlr4/blob/master/doc/getting-started.md)
- [Targeting C++](https://github.com/antlr/antlr4/blob/master/doc/cpp-target.md)
- [Example: ANTLR for C++ with CMake](https://beyondtheloop.dev/Antlr-cpp-cmake/)

## Documentation and Books

- [Documentation](https://github.com/antlr/antlr4/blob/4.6/doc/index.md)
- [The Definitive ANTLR4 Reference](https://docs.drlazor.be/pdf/The_Definitive_ANTLR4_Reference.pdf)
- [Leaning ANTLR (unofficial)](https://riptutorial.com/Download/antlr.pdf)