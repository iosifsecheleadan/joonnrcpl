# 3.4. Expressions

Expressions are any statement that has an explicit return value.

1. Value Expressions
2. Method Expressions
3. Arithmetic Expressions
4. Relational Expressions
5. Logical Expressions

## 3.4.1. Value

The following are examples of basic types. 

- Integers : `-12345`
- Float : `17.38`
- Character : `"?"`
- Boolean : `true` / `false`
- String : `"strings can be however long"`

These basic types are translated to Objects of the specified type.

## 3.4.2. Method or Constructor Call

Any method that is called is a method expression. It may have a value, or it 
may be a void method, in which case, it cannot be assigned.

Constructors can be thought of as returning an object of their own type. 

## 3.4.3. Arithmetic

Arithmetic expressions are binary operations between arithmetic types. They can 
only be applied to two arithmetic values of the same type (i.e. two Integers, 
two Floats, two Characters or any class extending the Arithmetic abstract). 

The compiler automatically casts Integers to Floats when doing arithmetic 
operations. 

Arithmetic operators are : 
```
+ // addition
- // subtraction
* // multiplication
/ // division
% // modulus - the rest of division
^ // power - raising to a power
```

See also Arithmetic Assignment

<!-- todo references to specified docs pages -->

## 3.4.4. Relational

Relational expressions are operations between Relational types. They can only be 
applied to two relational values of the same type (i.e. two Integers, two 
Floats, two Characters, two Strings or any class extending the Relational 
abstract.)

The compiler automatically casts Integers to Floats when doing relational 
operations.

Relational operators are:
```
< // smaller than
> // greater than
```

Note that `==` is a method of the Object type and should be implemented in any 
class. The `<=` and `>=` operators are then "auto-generated" so to speak, like 
so: 
```
(a <= b) == (a < b || a == b)
(a >= b) == (a > b || a == b)
```

## 3.4.5. Logical

Logical expressions are operations between Boolean types. Here you can find the 
only unary operator : `!`, and also two binary operators : `&&` and `||`.

```
! boolean // negates the boolean - true turns to false, false turns to true
boolean1 && boolean2 // true if both values are true, false if either value is false
boolean1 || boolean2 // true if either value is true, false if both values are false
```

Note that lazy evaluation applies. For the `&&` operator, if the first value is 
false, the second isn't evaluated. For the `||` operator, if the first value is 
true, the second isn't evaluated. 

