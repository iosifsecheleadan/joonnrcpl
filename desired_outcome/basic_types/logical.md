# 4.4. Logical

Logical is an abstract type. It doesn't hold any data, but simply defines 
the following abstract operations: 
- `&&` : `.and(other)`
- `||` : `.or(other)`
- `!` : `.not()`

