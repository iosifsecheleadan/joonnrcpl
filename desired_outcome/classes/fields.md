# 2.3. Fields

Any class may have none, one, or multiple fields. 

```
[private / protected / public] <fieldName> = defaultValue;
[private / protected / public] <fieldName> = defaultValueGetter();
```

## Field access modifiers

- Private: Only accessible from within the class
- Protected: Only accessible from classes extending this class
- Public: Accessible from any class
- Default: Public

## Field name

Should contain only alphanumerical characters and _. Must start with a letter.

Recommended to be camelCase starting with a lower case letter. 

## Initial assignment

Every field must be assigned by default. This assignment can then be 
overwritten in the constructor, but it must be initialized here first. 

This is a mechanism to avoid the necessity for null in the language. 

To learn more, see Assignment Statement and Expressions.

<!-- todo references to specific files -->