# 5. Collections

All collections are generic and can hold any type of data.

1. Iterable
2. List and DynamicArray
3. Deque and DoublyLinkedList
4. Set and HashSet
5. Map and HashMap
