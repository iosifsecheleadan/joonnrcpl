# 3.11. Throw

```
throw <Exception>
```

Any Exception can be thrown from anywhere in a method. An Exception is any 
object extending the Exception class.

When an exception is thrown, the current method execution is abruptly stopped. 
If the exception is thrown in a try block, then execution is continued by trying 
to match the exception to a catch clause. If it doesn't match a catch clause, 
or the exception is not in a try block, execution is continued from the place 
where the current method was called, again searching for a try block and then 
for catch clauses. If the exception is still not handled, it will again be 
thrown out to the next method, and so on. If the exception is not handled, even 
at the highest levels, the program exits with an exception. 