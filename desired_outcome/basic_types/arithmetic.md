# 4.2. Arithmetic

Arithmetic is an abstract type. It doesn't hold any data, but simply defines 
the following abstract operations: 
- `+` : `.plus(other)`
- `-` : `.minus(other)`
- `/` : `.dividedBy(other)`
- `*` : `.times(other)`
- `^` : `.toThePowerOf(other)`

The `%` : `.modulo(other)` is proper to the Integer type and is defined in the 
Integer class.