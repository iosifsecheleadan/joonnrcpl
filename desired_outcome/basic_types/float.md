# 4.6. Float

Float is a concrete implementation of the Arithmetic and Relational types.

Holds floating point numbers, that is, fractional numbers. 

Examples: -456765.349, -45.5678998, -0.876, 0.0, -0.0, 0.456, 56.9876, 3456765.987

## Underscores

Underscores can also be inserted into the numbers for visibility.

Valid: -1_234.567_89, 1_234.567_89, -1_2_3_4.5_6_7_8_9, -12_34.56_7_89

Invalid: _-123.45, -_123.45, -123_.45, -123._45, -123.45_

Constant auto-boxing applies 