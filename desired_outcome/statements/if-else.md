# 3.5. If Else

```
if (<boolean expression>) {
	[statements]
} [else {
	[statements]
}]
```

Note that the else, and all of the statements in between brackets are optional. 
The brackets themselves however, are not. 

Here is an example of nested if statements. It's not the pretties syntax, but 
with the help of associated brackets highlighting, there're no more confusion of 
which else belongs to what if. And, given that you use a consistent format, it's 
not terrible to read. What can I say, it's not great, not terrible.

```
if (boolean1) {
	// statements1
} else { if (boolean2) {
	// statements2
} else { if (boolean3) {
	// statements3
} else { if (boolean4) {
	// statements4
} else {
	// statements5
} } } } 
```