# ***jon***

jon is a shorthand of the acronym, JOONNRCPL, which stands for: 

- **J**oseph's 
- **O**bject **O**riented
- **N**on **N**ull
- **R**eference **C**ounting
- **P**rogramming **L**anguage

## why? 

Why a new programming language? 

Honestly, just because. This is a passion project. I wanted to exercise my 
coding skills and knowledge and create something awesome in the process. This is 
the result, or rather, the work in progress. 

## what? 

What is the philosophy behind ***jon***? 

The answer is in the name: 
- **Object Oriented** : 
	- "Everything is an Object" (to be read as - there are no primitives)
	- Single Responsibility principle: a class is either meant to be 
	instantiated (concrete class) or to provide functionality to other classes, 
	which can be done in two ways: with member fields or methods (abstract 
	class); or through utility methods (static class).
- **Non Null** : this really flows from the previous point. If an object is of 
type String, it can only hold Strings, not this random extra value that isn't of 
any type and yet can be assigned to any variable. Use the empty String if need 
be, and be free of NullPointerExceptions and null checks. 
- **Reference Checking** : every object keeps count of how many variables are 
holding on to it. When no more variables use the object, it is deleted. No 
manual memory management, nor garbage collectors. 

It is also completely open source under the MIT License. I have designed, 
written, tested and documented everything myself. 

<!-- todo reference to License document -->

## how? 

What tools did you use? 

I will use ANTLR for simplicity. I will be targeting C++ for speed and 
interoperability on different Operating Systems. 