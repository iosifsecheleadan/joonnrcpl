# 3.9. 

```
break
```

The break statement is used to abruptly exit a while or for loop. When reached, 
execution is resumed from the next statement after the current loop.