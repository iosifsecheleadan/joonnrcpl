# ***Jon*** Command Line Interface

---

Compile and run a program: 
```
jon -main <path/to/MainClassName>
	-indir <input/directory>[:<another/input/directory>]
	-outdir <compilation/output/directory>
```
All options are necessary, but can be provided in any order. Provide the `-nrun` 
option to also run the program, if successfully compiled.

---

Run a previously compiled program: 
```
jon -outdir <compilation/output/directory>
```

--- 

## OPTIONS

```
-indir <input/directory>[:<another/input/directory>]
```
Takes into consideration all `.jon` files in the given input directory or 
directories. Conflicting directories or files will result in a compilation 
error. Wrong paths will result in a compilation error.

---

```
-main <path/to/MainClassName>
```
The path to the main class is considered from the root of any one given input 
directory. It must have also have the `.jon` extension. If the file does not 
exist, compilation terminates with an error.

---

```
-nrun
```
This option runs also runs the program if compilation was successful.

---

```
-outdir <compilation/output/directory>
```
The output of the compilation will be written to this directory. If it does not 
exist, it will be created. If it exists, it will be overwritten (all contents 
are first deleted, then the compilation output is written). To run a previously 
compiled program, only use this option.

<!-- todo Option to compile to different targets (jvm, gcc, etc) ? -->

