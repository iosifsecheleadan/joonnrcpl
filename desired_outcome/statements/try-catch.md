# 3.12. Try Catch

```
try {
	[statements]
} catch(Exception1 [, Exception2, etc] <exceptionName>) {
	[statements]
} [ catch(Exception3 <exceptionName>) {
	[statements]
} [etc] 
]
```

The try catch statement is used to handle thrown exceptions. If an exception is 
thrown in the try block, then the it is matched to mentioned exception types in 
the first catch clause. If it matches any of them, the statements of that catch 
block are executed. Then all further catch clauses are checked and executed in 
the same manner. Multiple catch clauses can be executed for a single exception. 

Note that a break statement in a catch block will skip over all remaining catch 
clauses.