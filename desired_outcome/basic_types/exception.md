# 4.10. Exception

Exception is just a wrapper class around String. It is used to create custom 
Exceptions by extension. Only Exceptions may be thrown. 

```
class Exception {
	private String message = "";

	Exception(String message) {
		this.message = message;
	}

	String getMessage() {
		return message;
	}
}
```

Note that Errors are thrown directly by the compiler. These are compile time and 
cannot be caught. Exceptions are thrown at runtime and can be caught. 
