/* The state of a program is managed here while the actual data (i.e. object 
 * instances) of the program is stored in memory
 * Of course this datatbase should also be in memory
 * 
 * If an in memory darabase is not used, this should still be the representation 
 * of the program data
 */

create table Class (
	ClassID bigint NOT NULL auto_increment,
	FullName varchar(255) UNIQUE NOT NULL, /* package.name.ClassName */
	AccessModifier bit NOT NULL, /* 1 - public (default), 2 - protected, 
								  * 3 - private */
	ClassDescriptor bit NOT NULL, /* 1 - blank (i.e. concrete - deafult), 
								   * 2 - abstract, 3 - static */
	
	primary key (ClassID),
);
create unique index ClassFullName on Class (ClassFullName);

create table ClassImports (
	Importer bigint NOT NULL, 
	Imported bigint NOT NULL, 
	foreign key (Importer) references Class(ClassID),
	foreign key (Imported) references Class(ClassID),
);
create index ClassImporter on ClassImports (Importer);

create table ClassExtensions (
	Base bigint NOT NULL, 
	Super bigint NOT NULL,
	foreign key (Base) references Class(ClassID), 
	foreign key (Super) references Class(ClassID), 
);
create index ClassExtensionBase on ClassExtensions(Base);

/* actual objects are stored in memory 
 * reference counting
 */
create table Object (
	ObjectID bigint NOT NULL auto_increment,
	ObjectType bigint Not NULL,
	NumberOfReferencesToMe int NOT NULL,
	PointerInMemory int NOT NULL,
	primary key (ObjectID),
	foreign key (ObjectType) references Class(ClassID)
);

create table Field (
	FieldID bigint NOT NULL auto_increment,
	ClassID bigint NOT NULL,
	AccessModifier bit NOT NULL, /* see table Class */
	FieldName varchar(255) NOT NULL,
	FieldType bigint NOT NULL, 
	
	primary key (FieldID),
	unique (ClassID, FieldName),

	foreign key (ClassID) references Class(ClassID),
	foreign key (FieldType) references Class(ClassID),
); 
create unique index ClassFieldName on Field(ClassName, FieldName);

create table Method (
	MethodID bigint NOT NULL auto_increment,
	ClassID bigint NOT NULL,
	AccessModifier bit NOT NULL, /* see table Class */
	MethodDescriptor bit NOT NULL, /* 1 - blank (i.e. concrete - deafult), 
									* 2 - abstract, 3 - override */
	ReturnType bigint, /* NULL for void and constructors */
	MethodName varchar(255) NOT NULL, /* empty string for constructor */

	primary key (MethodID),
	unique (ClassID, MethodName),
	
	foreign key (ClassID) references Class(ClassID)
	foreign key (ReturnType) references Class(ClassID),
); 
create unique index ClassMethodName on Method(ClassID, MethodName);

create table Parameter (
	ParameterID bigint NOT NULL auto_increment,
	MethodID bigint NOT NULL,
	ClassID bigint NOT NULL,
	ParameterIndex bit NOT NULL,
	ParameterName varchar(255) NOT NULL,

	primary key (ParameterID),
	unique (MethodID, ParameterName),
	unique (MethodID, ParameterIndex),

	foreign key (MethodID) references Method(MethodID),
	foreign key (ClassID) references Class(ClassID),
);
create unique index MethodParameterName on Parameter(MethodID, ParameterName);
create unique index MethodParameterIndex on Parameter(MethodID, ParameterIndex);

/* holds local variables and instance variables (i.e. fields) */
create table Variable (
	VariableID bigint NOT NULL auto_increment,
	FieldID bigint, /* NULL for local variables */
	FieldOf bigint NOT NULL, /* NULL for local variables */
	ObjectID bigint NOT NULL, /* when setting new ObjectID, old.ObjectID must 
							   * match new.ObjectID*/
	variableName varchar(255), /* NULL for fields */

	primary key (VariableID),

	foreign key (FieldID) references Field(FieldID),
	foreign key (FieldOf) references Object(ObjectID),
	foreign key (ObjectID) references Object(ObjectID),
);