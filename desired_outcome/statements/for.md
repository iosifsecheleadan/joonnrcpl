# 3.7. For

```
for (<variableName> : <collection>) {
	[statements]
}
```

Pretty straight forward. The named variable is assigned, one by one, every value 
in the collection, and then it executes the statements in the code block.

For C - style for loops, you can use the following syntax.

```
i = 0; while (i <= 10) { i += 1; 
	[statements]
}
// OR, for 0 to 9 instead of 1 to 10
i = 0; while (i < 10) { 
	[statements]
i += 1; }
// OR
i = -1; while (i < 10) { i += 1;  
	[statements]
}
```

It's not very pretty, but it's not terrible either. 