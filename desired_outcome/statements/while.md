# 3.6. While

```
while (<boolean expression>) {
	[statements]
}
```

Pretty straight forward. While the boolean expression evaluates to true, the 
statements in the code block are executed.

<!-- todo do while statement -->