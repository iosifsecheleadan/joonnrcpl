# 3.13. The import statement

```
import <path.to.file.ClassName>
import <path.to.package.*>
```

Path to file should start from any directory mentioned with the `-indir` option.

Can import either specific class, or all classes in a package. 

There is no need to declare what package a class is part of. Directories 
automatically constitute packages.