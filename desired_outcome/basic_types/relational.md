# 4.8. Relational

Relational is an abstract type. It doesn't hold any data, but simply defines 
the following abstract operations: 
- `<` : `.smallerThan(other)`
- `>` : `.greaterThan(other)`

The `<=` : `.smallerOrEqual(other)` and `>=` : `.greaterOrEqual(other)` 
operators are defined as: 
```
(a <= b) == (a < b || a == b)
(a >= b) == (a > b || a == b)
```