# 4.7. Boolean

Boolean is a concrete implementation of the Logical type.

Boolean variables hold only the value `true` or `false`.
