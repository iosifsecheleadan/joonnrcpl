# 4.1. Object

All classes are subclasses of Object class. Object is a concrete class. It has 
two methods:
```
public String toString()
public Boolean equals(Object other)
```

The toString method returns a String representing the Object, in this case it's 
type and memory address. This behavior can be changed by overriding the method.

The equals method compares the memory locations of the two objects. This 
behavior can be changed by overriding the method.

The object class also implements Reference Counting privately. But that 
mechanism works silently, in the background. 