# 2.2. Enumeration

An enumeration is an optional part of any class.

```
[private/protected/public] enumeration[(Type)] {
	LABEL1 [(value1)],
	LABEL2 [(value2)],
}
```

## Enumeration access modifier

- Private: Only accessible from within the class
- Protected: Only accessible from classes extending this class
- Public: Accessible from any class
- Default: Public

## Enumeration LABELS and associated values

Should contain only alphanumerical characters and _. Must start with a letter.

Recommended to use SCREAMING_SNAKE_CASE.

Every enumeration label has an index (according to declaration order, starting 
from 0) and may have an optional value associated to it. This additional value 
must be of the declared type. 

All enumerations of a class are class constants and are part of the generic 
enumeration<ClassName> type.

## Enumeration access syntax

```
ClassName.LABEL1
ClassName.LABEL1.index()
ClassName.LABEL1.value()	// if the optional type is omitted, the index is 
							// returned
for (enum in ClassName.enumeration()) {	// if there is no enumeration, 
										// returns an empty list
	enum.index()
	enum.value()
}	
```
